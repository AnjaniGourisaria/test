"""
Module for performing a ping sweep, detecting live hosts on a network.
"""

import platform
import subprocess
import ipaddress
import re
from typing import List, Optional
from pydantic import BaseModel

class NetworkInfo(BaseModel):
    """Pydantic Datamodel: Network Information."""
    ip_address: Optional[str] = None
    subnet_mask: Optional[str] = None

    def __str__(self):
        """Returns a string representation of the network information."""
        return f"IP Address: {self.ip_address}, Subnet Mask: {self.subnet_mask}"


class PingSweepResult(BaseModel):
    """Pydantic Datamodel: Ping Sweep Result segregated by each network interface."""
    results: List[dict] = []

    def add_result(self, network_info: NetworkInfo, network_range: str, live_hosts: List[str]) -> None:
        """Add a result for a specific network interface."""
        self.results.append({
            "network_info": network_info,
            "network_range": network_range,
            "live_hosts": live_hosts
        })

    def __str__(self):
        """String Representation of Ping Sweep results for all interfaces."""
        result_str = "Ping Sweep Results:\n"
        for result in self.results:
            network_info = result["network_info"]
            network_range = result["network_range"]
            live_hosts = result["live_hosts"]
            live_hosts_str = ', '.join(live_hosts) if live_hosts else 'None'
            result_str += (
                f"\nNetwork Info: {network_info}\n"
                f"Network Range: {network_range}\n"
                f"Live Hosts: {live_hosts_str}\n"
            )
        return result_str


class PingSweep:
    """Handles detection of network details and performs ping sweep."""
    def __init__(self) -> None:
        """Initialize: Setting the network information to None."""
        self.network_info = None

    def get_ip_and_subnet(self) -> None:
        """Detects IP address and subnet mask based on OS."""
        os_type = platform.system()
        if os_type == "Windows":
            return self.get_ip_and_subnet_windows()
        elif os_type == "Linux":
            return self.get_ip_and_subnet_linux()
        elif os_type == "Darwin":
            return self.get_ip_and_subnet_mac()
        else:
            raise RuntimeError(f"Unsupported OS: {os_type}")

    def get_ip_and_subnet_windows(self) -> None:
        """Windows: Fetch IP and subnet mask for all interfaces, including virtual ones."""
        try:
            result = subprocess.run(
                "ipconfig",
                capture_output=True,
                text=True,
                check=False
            )
            ip_pattern = re.compile(r"IPv4 Address.*?:\s*(\d+\.\d+\.\d+\.\d+)")
            mask_pattern = re.compile(r"Subnet Mask.*?:\s*(\d+\.\d+\.\d+\.\d+)")

            all_network_info = []
            ip_address = None
            subnet_mask = None
            for line in result.stdout.splitlines():
                ip_match = ip_pattern.search(line)
                mask_match = mask_pattern.search(line)
                if ip_match:
                    ip_address = ip_match.group(1)
                if mask_match:
                    subnet_mask = mask_match.group(1)
                if ip_address and subnet_mask:
                    all_network_info.append(NetworkInfo(ip_address=ip_address, subnet_mask=subnet_mask))
                    ip_address = None
                    subnet_mask = None

            if all_network_info:
                self.network_info = all_network_info
            else:
                raise RuntimeError("No valid network interfaces found.")
        except Exception as e:  # pylint: disable=W0718:broad-exception-caught
            raise RuntimeError(f"Error obtaining IP address on Windows: {e}") from e

    def get_ip_and_subnet_linux(self) -> None:
        """Linux: Fetch IP and subnet mask for all interfaces, including virtual ones."""
        try:
            result = subprocess.run(
                "ip -o -f inet addr show scope global",
                capture_output=True,
                text=True,
                shell=True,
                check=False
            )
            all_network_info = []

            for line in result.stdout.splitlines():
                tokens = line.strip().split()
                iface = tokens[1]
                if iface != 'lo':
                    ip_with_prefix = tokens[3]
                    ip_address, prefix_length = ip_with_prefix.split('/')
                    subnet_mask = str(ipaddress.IPv4Network(f"0.0.0.0/{prefix_length}").netmask)
                    all_network_info.append(NetworkInfo(ip_address=ip_address, subnet_mask=subnet_mask))

            if all_network_info:
                self.network_info = all_network_info
            else:
                raise RuntimeError("No valid network interfaces found.")
        except Exception as e:  # pylint: disable=W0718:broad-exception-caught
            raise RuntimeError(f"Error obtaining IP address on Linux: {e}") from e

    def get_ip_and_subnet_mac(self) -> None:
        """MacOS: Fetch IP and subnet mask for all interfaces, including virtual ones."""
        try:
            result = subprocess.run(
                "ifconfig",
                capture_output=True,
                text=True,
                shell=True,
                check=False
            )
            lines = result.stdout.split('\n')
            iface = None
            all_network_info = []

            for line in lines:
                if line and not line.startswith('\t') and not line.startswith(' '):
                    iface_line = line.strip()
                    iface = iface_line.split(':')[0]
                    if 'lo' in iface.lower() or 'loopback' in iface.lower():
                        iface = None
                        continue
                elif iface:
                    if 'inet ' in line and 'broadcast' in line:
                        ip_line = line.strip()
                        tokens = ip_line.split()
                        ip_index = tokens.index('inet')
                        ip_address = tokens[ip_index + 1]
                        netmask_index = tokens.index('netmask')
                        subnet_mask_hex = tokens[netmask_index + 1]
                        subnet_mask = '.'.join(
                            str(int(subnet_mask_hex[i:i+2], 16))
                            for i in range(2, len(subnet_mask_hex), 2)
                        )
                        all_network_info.append(NetworkInfo(ip_address=ip_address, subnet_mask=subnet_mask))

            if all_network_info:
                self.network_info = all_network_info
            else:
                raise RuntimeError("No valid network interfaces found.")
        except Exception as e:  # pylint: disable=W0718:broad-exception-caught
            raise RuntimeError(f"Error obtaining IP address on MacOS: {e}") from e

    def calculate_network_ranges(self) -> list:
        """Calculates network ranges for all detected IPs and subnet masks."""
        if not self.network_info:
            raise ValueError("IP address or subnet mask is missing!")

        network_ranges = []
        for net_info in self.network_info:
            ip_interface = ipaddress.IPv4Interface(
                f"{net_info.ip_address}/{net_info.subnet_mask}"
            )
            network_ranges.append(str(ip_interface.network))
        return network_ranges

    def ping_sweep(self, network_range: str) -> list:
        """
        Performs a ping sweep on the network range.
        
        Args:
            network_range (str): CIDR Notation
        """
        try:
            result = subprocess.run(
                ["nmap", "-sn", network_range],
                capture_output=True,
                text=True,
                check=False
            )
            # print(result)
            live_hosts = []
            for line in result.stdout.splitlines():
                if "Nmap scan report for" in line:
                    host_ip = line.split()[-1].strip("()")
                    live_hosts.append(host_ip)
            return live_hosts
        except FileNotFoundError as e:
            raise FileNotFoundError("Nmap is not installed or not found in system's PATH.") from e


class PingSweepController:
    """Controller to handle the ping sweep execution."""
    def run(self) -> None:
        """ Run: Ping Sweep """
        ping_sweep = PingSweep()
        result = PingSweepResult()

        try:
            ping_sweep.get_ip_and_subnet()
            if not ping_sweep.network_info:
                raise RuntimeError("Could not detect any IP address or subnet mask.")

            network_ranges = ping_sweep.calculate_network_ranges()

            # Perform ping sweep for each network range and store results
            for i, network_range in enumerate(network_ranges):
                print(f"Sweeping {network_range}")
                if network_range == '172.17.0.0/16':
                    continue
                if network_range == '10.0.3.0/24':
                    continue
                live_hosts = ping_sweep.ping_sweep(network_range)
                network_info = ping_sweep.network_info[i]
                result.add_result(network_info, network_range, live_hosts)

            # Print the organized results
            print(result)

        except Exception as e:  # pylint: disable=W0718:broad-exception-caught
            raise RuntimeError(f"Error during Ping Sweep: {e}") from e

if __name__ == "__main__":
    PingSweepController().run()
